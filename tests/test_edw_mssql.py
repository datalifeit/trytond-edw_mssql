# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class ExternalEdwMssqlTestCase(ModuleTestCase):
    """Test External Edw Mssql module"""
    module = 'edw_mssql'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ExternalEdwMssqlTestCase))
    return suite
