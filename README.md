datalife_edw_mssql
===========================

The edw_mssql module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-edw_mssql/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-edw_mssql)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
